#+TITLE: Requirements for Binary Search Tree experiment
#+AUTHOR: VLEAD
#+DATE: [2016-07-1 Sat]
#+PROPERTY: results output
#+PROPERTY: exports code
#+SETUPFILE: ../org-templates/level-1.org
#+options: ^:nil
#+LATEX: Literal LaTeX code for export

* Introduction 
  The requirements of =Binary Search Tree experiment= are listed here.

** Requirements
   - Make an interactive lab for binary search trees by rendering templates.
   - The interactive lab should teach and demonstrate operations searching, inserting, deleting on the binary search tree.
   - Give exercises for building BST and then searching and deleting an element to be done by the users.
   - Give exercises for tree traversal of BST and its types.
   - Add time complexity demonstration of the BST search algorithm.
